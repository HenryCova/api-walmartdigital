package com.desafio.walmartdigital.api.rest.model;

import java.io.Serializable;

import org.springframework.boot.autoconfigure.domain.EntityScan;
//import javax.persistence.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@EntityScan
@Getter
@Setter
@NoArgsConstructor
@Document(collection  = "products")
public class Product implements Serializable{

	private static final long serialVersionUID = 6040406818520072701L;

	@Id
	private Long id;
	
	private String brand;
	
	private String description;
	
	private String image;
	
	private Long price;
	
	public Product() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}
	
	
}   
