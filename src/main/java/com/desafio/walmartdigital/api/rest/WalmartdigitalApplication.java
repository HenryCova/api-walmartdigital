package com.desafio.walmartdigital.api.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WalmartdigitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(WalmartdigitalApplication.class, args);
	}

}
