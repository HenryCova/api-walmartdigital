package com.desafio.walmartdigital.api.rest.controller;


import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.walmartdigital.api.rest.model.Product;
import com.desafio.walmartdigital.api.rest.service.IProductService;

@CrossOrigin()
@RestController
@RequestMapping("/api")
public class ProductController {
	
	private static final int PAGE =  0;
    private static final Logger LOG = LoggerFactory.getLogger(ProductController.class);
    private static Integer CONSTAN_PAGE = 3; 
	
	@Autowired
	private IProductService productServices; 

		@GetMapping("/products/page/{page}")
		public ResponseEntity<?> allProductosBy(
				@PathVariable int page,
				@RequestParam("candidate") String candidate){	
			
			Map<String, Object> response = new HashMap<>();
			Page<Product> productos ;
			
			try {
				LOG.info("Iniciando Busqueda de productos.");
				productos = productServices.findAll(
						candidate ,PageRequest.of(page,CONSTAN_PAGE.intValue())) ;
				
			}catch(DataAccessException e) {
				LOG.error("Error al realizar la consulta en la base de datos");
				LOG.error( "error:::" + HttpStatus.INTERNAL_SERVER_ERROR);
				response.put("mensaje", "Error al realizar la consulta en la base de datos");
				response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			LOG.info("Exito buscando todos los productos.");

			return new ResponseEntity<Page<Product>>(productos, HttpStatus.OK);
		}
		
		@GetMapping("/{productoId}")
		public  ResponseEntity<?> ProductosPorId(
				@PathVariable Long productoId){	
			
			Map<String, Object> response = new HashMap<>();
			Page<Product> productos ;
			
			try {
				LOG.info("Iniciando Busqueda de productos.");
				productos = productServices.findById(
						productoId ,PageRequest.of(PAGE,CONSTAN_PAGE.intValue()));
				
			}catch(DataAccessException e) {
				LOG.error("Error al realizar la consulta en la base de datos");
				LOG.error( "error:::" + HttpStatus.INTERNAL_SERVER_ERROR);
				response.put("mensaje", "Error al realizar la consulta en la base de datos");
				response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			LOG.info("Exito buscando el productos: " + productoId);

			return new ResponseEntity<Page<Product>>(productos, HttpStatus.OK);
					
		}
	  
		
}
