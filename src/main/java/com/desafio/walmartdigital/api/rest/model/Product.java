package com.desafio.walmartdigital.api.rest.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection  = "products")
public class Product implements Serializable{

	private static final long serialVersionUID = 6040406818520072701L;

	@Id
	private String _id;
	
	private String id;
		
	private String brand;
	
	private String description;
	
	private String image;
	
	private Long price;
	
	public Product() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}
	
	public boolean palindrome( String myPhrase) {
		
		myPhrase = myPhrase.toLowerCase().replace(" ", "").replace(",","");       
		int counter = 0, longWord = myPhrase.length()-1;
		boolean isError = false;

		while ((counter<longWord) && (!isError)){
		    if (myPhrase.charAt(counter) == myPhrase.charAt(longWord)){             
		        counter++;
		        longWord--;
		    } else {
		        isError = true;
		    }
		}
		if (!isError){
		    System.out.println(myPhrase + " : es un PALÍNDROMO :-)");
		    return true;
		}else{
		    System.out.println(myPhrase + " : NO es un PALÍNDROMO :-(");
		    return false;
		}  
	} 
	
}   
