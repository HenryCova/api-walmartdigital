package com.desafio.walmartdigital.api.rest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.desafio.walmartdigital.api.rest.model.Product;


public interface IProductRepository extends MongoRepository<Product, String>{
	
	
	/**
	 * Busca producto por Id y retona una lista con el producto 
	 * @param id
	 * @param pageable
	 * @return
	 */
	public Page<Product> findById(Long id , Pageable pageable);
	
	
	/**
	 * 
	 * @param id
	 * @param Brand
	 * @param description
	 * @param pageable
	 * @return
	 */
	public Page<Product> findByIdOrDescriptionLikeOrBrandLike(
			String id ,String Brand ,String description , Pageable pageable);
	
}
	