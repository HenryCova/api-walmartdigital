package com.desafio.walmartdigital.api.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.desafio.walmartdigital.api.rest.model.Product;

public interface IProductService {

	/**
	 * Busca todos los productos y retona una lista con todos.
	 * @param productoId
	 * @param pageable
	 * @return
	 */
	public Page<Product> findById( Long productoId , Pageable pageable);
	
	/**
	 * 
	 * @param candidate
	 * @param pageable
	 * @return
	 */
	public Page<Product> findAll(String candidate, Pageable pageable);
	
}
