package com.desafio.walmartdigital.api.rest.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.desafio.walmartdigital.api.rest.model.Product;
import com.desafio.walmartdigital.api.rest.repository.IProductRepository;
import com.desafio.walmartdigital.api.rest.service.IProductService;


@Service
public class ProductServiceImpl implements IProductService {

    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
    
    @Autowired
	private IProductRepository productRepository;

	public Page<Product> findById(Long productoId , Pageable pageable) {
		log.info("Buscando producto por Id :" + productoId );
		return productRepository.findById(productoId, pageable);

	}

	@Override
	public Page<Product> findAll(String candidate, Pageable pageable) {
		
		Page<Product> Productos;
		
		if(candidate.isEmpty()){
			log.info("Buscando todos los productos");
			Productos =  productRepository.findAll(pageable);
		}else{
			log.info("Buscando por el candidato : " + candidate  );
			Productos = productRepository.findByIdOrDescriptionLikeOrBrandLike(
					candidate, candidate, candidate , pageable);
		}
	
		return Productos; 
	}

}
