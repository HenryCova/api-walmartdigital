package com.desafio.walmartdigital.api.rest.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;

import com.desafio.walmartdigital.api.rest.controller.ProductController;
import com.desafio.walmartdigital.api.rest.service.IProductService;


@RunWith(MockitoJUnitRunner.class)
public class TestProductController {
	
	@InjectMocks
	private ProductController productController;
	
	@Mock
	private IProductService productServices; 

	/**
	 * Camino feliz
	 */
	 @Test
	 public void testAllProductosBy() {
		 		 
		 int page = 0;
		 String candidate = "hola";
		 
		 when (productServices.findAll( any(), any())).thenReturn(null);
		 
		 ResponseEntity<?> salida =productController.allProductosBy(page, candidate);
		 
		 assertEquals("200 OK", salida.getStatusCode().toString());
		 assertNull(salida.getBody());
		 
	   }
	 
	 /**
	  * Camino DataAccessException
	  */
	 @Test
	 public void testAllProductosByNoK() {
		 		 
		 int page = 0;
		 String candidate = "hola";
		 
		 when (productServices.findAll( any(), any())).thenThrow(new DataAccessException("..."){ } );
		 
		 ResponseEntity<?> salida =  productController.allProductosBy(page, candidate);
		 
		 assertEquals("500 INTERNAL_SERVER_ERROR", salida.getStatusCode().toString());
		 
	   }

	 /**
	  * Camino feliz
	  */
	 @Test
	 public void testProductosPorId() {
		 		 
		 Long productoId = 181L;
		 
		 when (productServices.findById( any(), any())).thenReturn(null);
		 
		 ResponseEntity<?> salida =productController.ProductosPorId(productoId);
		 
		 assertEquals("200 OK", salida.getStatusCode().toString());
		 assertNull(salida.getBody());
		 
	   }
	 
	 /**
	  * Camino DataAccessException
	  */
	 @Test
	 public void testProductosPorIdNoK() {
		 		 
		 Long productoId = 181L;
		 
		 when (productServices.findById( any(), any())).thenThrow(new DataAccessException("..."){ } );
		 
		 ResponseEntity<?> salida = productController.ProductosPorId(productoId);
		 
		 assertEquals("500 INTERNAL_SERVER_ERROR", salida.getStatusCode().toString());
		 
	   }
	 
	 
	
}
