package com.desafio.walmartdigital.api.rest.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestProduct {
	 
	
	@Test
	public void testProduct() {
	
		Product produc = new Product();
		
		
		produc.set_id("123");
		produc.setBrand("Marca");
		produc.setDescription("description");
		produc.setId("1");
		produc.setImage("image");
		produc.setPrice(20000L);
		
		
		assertEquals("123", produc.get_id());
		assertEquals("Marca", produc.getBrand());
		assertEquals("description", produc.getDescription());
		assertEquals("1", produc.getId());
		assertEquals("image", produc.getImage());
		assertEquals(20000L, produc.getPrice(), 1);
		
		
		assertEquals(true, 	produc.palindrome("aba"));
		assertEquals(false,	produc.palindrome("abb"));

		
	
		
	}

}
