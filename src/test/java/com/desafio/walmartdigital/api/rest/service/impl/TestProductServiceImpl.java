package com.desafio.walmartdigital.api.rest.service.impl;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.desafio.walmartdigital.api.rest.model.Product;
import com.desafio.walmartdigital.api.rest.repository.IProductRepository;


@RunWith(MockitoJUnitRunner.class)
public class TestProductServiceImpl {

	@InjectMocks
	private ProductServiceImpl productServiceImpl;
	
	@Mock
	private IProductRepository productRepository;
	
	
	 @Test
	 public void testfindById() {
		 
		 when(productRepository.findById(any(),any())).thenReturn(null);
		 
		 productServiceImpl.findById( 1L,  PageRequest.of(0,4));
	 }
	 
	 /**
	  * Candidato con valor 
	  */
	 @Test
	 public void testfindAll() {
		 String candidate = "brh";
		 
		 when(productRepository.findByIdOrDescriptionLikeOrBrandLike(
					any(), any(), any(), any())).thenReturn(null);
		 
		 Page<Product> salida =productServiceImpl.findAll(candidate, PageRequest.of(0,4)); 
		 
		 assertNull(salida);
	 }
	 
	 /**
	  * Candidato con valor nulo 
	  */
	 @Test
	 public void testfindAllEmpy() {
		 String candidate = "";

		 when(productRepository.findAll( PageRequest.of(0,4))).thenReturn(null);
		 
		 Page<Product> salida = productServiceImpl.findAll(candidate, PageRequest.of(0,4));
		 
		 assertNull(salida);
	 }
	 
	
}
